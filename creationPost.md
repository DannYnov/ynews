---
layout: page
title: Créer un post
permalink: /creationPost/
---

### Titre du post
  <form>
    <input id="titrePost" type="text"/>
  </form>
### Auteur du post
  <form>
    <input id="authorPost" type="text"/>
  </form>
### Contenu

  <form>
      <div class="form-group">
        <small class="form-text text-muted">en markdown</small>
        <textarea class="form-control" id="textPost" rows="3"></textarea>
      </div>
      <button type="submit" id="submit" onclick="createArticle()" class="btn btn-primary">Creer le post</button>
      <button class="btn btn-secondary" value="save" id="save" onclick="downloadPost()">Télécharger le post</button>
  </form>
  <h1>Ajouter une photo </h1>
  <button class="btn btn-primary" onclick="startVideo();">Start Vidéo</button>
  <button class="btn btn-primary" onclick="takePicture();">Prendre une photo</button>
  <button class="btn btn-primary" onclick="stopVideo();">Arrêter la caméra</button>
  <video id="video"></video>
  <canvas id="canvas"></canvas>
