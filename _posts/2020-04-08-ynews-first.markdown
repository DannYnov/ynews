---
layout: post
title:  "Votre nouvelle PWA est sortie !"
date:   2020-04-08 13:14:15 -0300
categories: Ynovaix
imgurl: "https://ressources.blogdumoderateur.com/2019/06/pwa-logo-664x304.jpg"
---

Premier post de notre startup Ynews !

Voici une image :

 ![Ynov Informatique]({{ site.baseurl }}/assets/images/logo_ynov.jpg)

 {% include icon-twitter.html username=site.twitter_username %}
