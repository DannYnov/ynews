---
layout: post
title:  "Notre nouvelle PWA"
date:   2016-03-24 14:00:00 -0300
author: Danny Brillant
categories: Ynovaix
imgurl: "https://www.ynov-aix.com/app/uploads/2019/11/ynov-animation.svg"
---


Premier post de notre startup Ynews !

Voici une image :

 ![Ynov Informatique]({{ site.baseurl }}/assets/images/logo_ynov.jpg)

 {% include icon-twitter.html username=site.twitter_username %}
