YNEWS

Ynews est une application qui permettra à tout les acteurs de l'ecole de se tenir au courant des différentes informations concernant le campus.
Ainsi, l'administration et les différents bureaux étudiants de tenir les étudiants et les intervenants au courant des actualités.
De plus, Les élèves pourront créer des groupes afin d'échanger les informations sur leur cours ou leur projet. 
Et les intervenants pourront transmettre les actualités concernant leur cours à leurs élèves.

Lien vers le client déployé :

https://dannynov.gitlab.io/ynews/creationPost/

Liens vers les différentes page du wiki :

https://gitlab.com/DannYnov/ynews/-/wikis/Phase-Conception-Groupe-SCM

https://gitlab.com/DannYnov/ynews/-/wikis/Phase-de-découverte---Groupe-SCM

Lien vers la documentation du back :

https://documenter.getpostman.com/view/6662444/SzmiYH2K?version=latest