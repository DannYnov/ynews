const CACHE_NAME = 'ynews-cache-v2';

var FILES_TO_CACHE = [
    './',
    './index.html',
    './about/index.html',
    './creationPost/index.html',
    './login/index.html',
    './assets/images/logo_ynews.png',
    './assets/js/script.js',
    './assets/css/bootstrap/bootstrap.min.js',
    './assets/css/main.css',
    './assets/css/bootstrap/bootstrap.min.css'
];

 //--------------INSTALL---------------------------
self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  evt.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page');
      return cache.addAll(FILES_TO_CACHE);
    })
  );
  self.skipWaiting();
});

 //--------------ACTIVATE---------------------------
self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  evt.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key !== CACHE_NAME) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  self.clients.claim();
  });

//--------------FETCH---------------------------
self.addEventListener('fetch', (evt) => {
  console.log('[ServiceWorker] Fetch', evt.request.url);
  evt.respondWith(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.match(evt.request)
      .then((response) => {
        return response || fetch(evt.request);
      });
    })
  );
});
