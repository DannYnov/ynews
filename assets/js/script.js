
var connected = false;
//function pour creer le file sur le server
function WriteToFile(){
  var fso = new ActiveXObject("Scripting.FileSystemObject");
  var s = fso.CreateTextFile("./_posts/"+getPostTitle()+".markdown", true);
  s.WriteToFile(saveTextPost());
  s.Close();
}

//function pour recuperer le contenue du post dans la textArea
function saveTextPost() {
  var textToWrite = document.getElementById('textPost').value;
  textToWrite = textToWrite.replace(/\n/g, "\r\n");
  var title = document.getElementById('titrePost').value;
  var author = document.getElementById('authorPost').value;
  var file;
  file.author = author;
  file.title = title;
  file.content = new Blob([ textToWrite ], { type: 'text/markdown' });
  file.name=getPostTitle();
  return file;
}

//function pour recupérer le titre
function getPostTitle(){
  var date =new Date();
  var titre = document.getElementById('titrePost').value;
  var name = new String(date.toISOString().substring(0,10)+'-'+ titre);
  return name;
}

//function pour dowload post
function downloadPost(){
  var downloadLink = document.createElement("a");
  downloadLink.download = getPostTitle()+".markdown";
  downloadLink.innerHTML = "Download File";
  if (window.webkitURL != null) {
    // Chrome allows the link to be clicked without actually adding it to the DOM.
    downloadLink.href = window.webkitURL.createObjectURL(saveTextPost());
  } else {
    // Firefox requires the link to be added to the DOM before it can be clicked.
    downloadLink.href = window.URL.createObjectURL(saveTextPost());
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
  }

  downloadLink.click();
}

//detruit le lien de download
function destroyClickedElement(event) {
  document.body.removeChild(event.target);
}

function createArticle(){

  var file = saveTextPost();
  var jsonRequest = JSON.stringfy({title:file.title, content: file.name, author : file.author});
  jQuery.ajax({
    type: "POST",
    url: 'adresseDuServer/ArticleController.php',
    dataType: 'json',
    data: {functionname: 'createarticle', arguments: [jsonRequest, ""]},
});
}

function connexion(){
  var usernameInput = document.getElementById('exampleInputEmail1').value;
  var passwordInput = document.getElementById('exampleInputPassword1').value;
  var jsonRequest = JSON.stringfy({username:usernameInput, password: passwordInput});
  jQuery.ajax({
    type: "POST",
    url: 'adresseDuServer/AuthController.php',
    dataType: 'json',
    data: {functionname: 'api', arguments: [jsonRequest, ""]},
});
}

//var buttonCreate = document.getElementById('submit');
//buttonCreate.addEventListener('click', WriteToFile());

//var buttonDownload = document.getElementById('save');
//buttonDownload.addEventListener('click', downloadPost());
