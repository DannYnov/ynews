---
layout: page
title: A Propos
permalink: /about/
---

<div class="container">
  <div class="row">
    <div class="col-md-12 center">
      <div class="row">
        <div class="col-md-6 center"> 
          <h2>   Histoire </h2>
          Ynews vous permettra de tout savoir sur l'école Ynov - Aix en provence.
          De ces événements à ces différents défi proposé à ces étudiants.
          Vous saurez tout !
        </div> 
        <div class="col-md-6 center"> 
          <h2>   Citation </h2>
          “Aimer l'actualité, c'est faire corps avec elle.” <b><i>Michel Field*</i></b>
        </div> 
      </div>
<br><br>
      <div class="col-md-12 center">
        <h1>  L'école </h1>
        <br>
      </div>
      <div class="row">
        <div class="col-md-6 center"> 
        <img class="small-img-test" src="{{ site.baseurl }}/assets/images/logo_ynov_campus_aix.png"/>
        </div>
        <div class="col-md-6 center"> 
          <h3><b><i> Mais où est l'école ? </i></b></h3>
          <div id="locecole">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2892.982398523226!2d5.430553615719909!3d43.523559768821634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12c98d742890b22f%3A0xdb074cd9a8f6b7e0!2sAix%20Ynov%20Campus!5e0!3m2!1sfr!2sus!4v1587647346129!5m2!1sfr!2sus" width="auto" height="auto" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>
        </div>
      </div>
<br><br>
      <div class="col-md-12 center"> 
        <h1>  L'équipe de développement</h1>
        <br>
        <div class="row">
          <div class="col">
            <div class="card" style="width: 18rem;">
              <img src="https://media-exp1.licdn.com/dms/image/C4E03AQHgQSOIDWMMPg/profile-displayphoto-shrink_800_800/0?e=1591833600&v=beta&t=QzkG6EQjVNt4706r6l733MGxj-F0lcGGm4iQE1HGj5U" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Cyriel</h5>
                <p class="card-text">Un développeur web en Or.
                  <i>"Je suis Chef De Projet pas dev!!!"</i></p>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card" style="width: 18rem;">
              <img src="https://media-exp1.licdn.com/dms/image/C5603AQH8piWnpCTAVQ/profile-displayphoto-shrink_200_200/0?e=1591833600&v=beta&t=tw52E96s6OtKRJ0xlMNu0epNt2hxgUg4kj5cQYtn5rI" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Antoine</h5>
                <p class="card-text">Un Developpeur non Web.
                  <i>"Faut faire tout ça ?"</i></p>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card" style="width: 18rem;">
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTYIoUhG49wy_YIzrlNtKZlMhA_KZ-qyV5i-j9uRBQSqI728RJd&usqp=CAU" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Danny</h5>
                <p class="card-text">Un designer Web de génie.
                  <i>"Tu connais un fille célib ?"</i></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
