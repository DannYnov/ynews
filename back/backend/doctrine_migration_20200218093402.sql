-- Doctrine Migration File Generated on 2020-02-18 09:34:02

-- Version 20200218093041
ALTER TABLE contracts ADD contract_id VARCHAR(50) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (contract_id);
INSERT INTO migration_versions (version, executed_at) VALUES ('20200218093041', CURRENT_TIMESTAMP);

-- Version 20200218093143
CREATE TABLE contracts (contract_id VARCHAR(50) NOT NULL, label VARCHAR(50) NOT NULL, contractnumber VARCHAR(50) NOT NULL, pdvref VARCHAR(50) NOT NULL, marchandref VARCHAR(100) NOT NULL, PRIMARY KEY(contract_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
INSERT INTO migration_versions (version, executed_at) VALUES ('20200218093143', CURRENT_TIMESTAMP);

-- Version 20200218093146
CREATE TABLE contracts (contract_id VARCHAR(50) NOT NULL, label VARCHAR(50) NOT NULL, contractnumber VARCHAR(50) NOT NULL, pdvref VARCHAR(50) NOT NULL, marchandref VARCHAR(100) NOT NULL, PRIMARY KEY(contract_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
INSERT INTO migration_versions (version, executed_at) VALUES ('20200218093146', CURRENT_TIMESTAMP);
