Run before launch :

Create database :  php bin/console doctrine:database:create
Create migration : php bin/console make:migration
Make migration : php bin/console doctrine:migrations:migrate -n
Run : php bin/console serve:run
