<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200514111229 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contracts (contract_id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, contractnumber VARCHAR(50) NOT NULL, pdvref VARCHAR(50) NOT NULL, marchandref VARCHAR(100) NOT NULL, cardtype VARCHAR(50) NOT NULL, PRIMARY KEY(contract_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE CustomPageCode (custom_page_codecode VARCHAR(255) NOT NULL, custom_page_codelabel VARCHAR(200) NOT NULL, custom_page_codetype VARCHAR(100) NOT NULL, custom_page_codemarchandref VARCHAR(100) NOT NULL, custom_page_codepdvref VARCHAR(50) NOT NULL, PRIMARY KEY(custom_page_codecode)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE merchands (id VARCHAR(20) NOT NULL, name VARCHAR(25) NOT NULL, access_key VARCHAR(100) NOT NULL, merchandenv VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_86B5A4915E237E06 (name), UNIQUE INDEX UNIQ_86B5A491EAD0F67C (access_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pointofsell (posid INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, marchandref VARCHAR(100) NOT NULL, PRIMARY KEY(posid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transactions (transaction_id VARCHAR(50) NOT NULL, marchandref VARCHAR(50) NOT NULL, contractnumber VARCHAR(50) NOT NULL, refcmd VARCHAR(50) NOT NULL, amount VARCHAR(100) NOT NULL, actioncode VARCHAR(100) NOT NULL, transactionstatus VARCHAR(100) NOT NULL, PRIMARY KEY(transaction_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contracts');
        $this->addSql('DROP TABLE CustomPageCode');
        $this->addSql('DROP TABLE merchands');
        $this->addSql('DROP TABLE pointofsell');
        $this->addSql('DROP TABLE transactions');
    }
}
