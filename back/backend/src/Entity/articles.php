<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Table(name="articles")
 * @ORM\Entity
 */
class articles

{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=60)
     */
    private $articleTITLE;
    /**
     * @ORM\Column(type="text")
     */
    private $articCONTENT;
    /**
     * @ORM\Column(type="string",length=70)
     */
    private $articleAUTOR;
    /**
     * @ORM\Column(type="date")
     */
    private $articleDATE;
    public function __construct($articleTITLE, $articCONTENT, $articleAUTOR, $articleDATE)
    {
        $this->articleTITLE = $articleTITLE;
        $this->articCONTENT = $articCONTENT;
        $this->articleAUTOR = $articleAUTOR;
        $this->articleDATE = $articleDATE;
    }
    public function getArticles()
    {
        return $this->articleTITLE;
    }
}