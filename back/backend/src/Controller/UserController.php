<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\User;
use App\Form\UserType;
use Psr\Log\LoggerInterface;
/**
 * User controller.
 * @Route("/api", name="api_")
 */
class UserController extends FOSRestController
{
  /**
   * Lists all User.
   * @Rest\Get("/Users")
   *
   * @return Response
   */
  public function getUserAction()
  {
    $repository = $this->getDoctrine()->getRepository(User::class);
    $Users = $repository->findall();
    return $this->handleView($this->view($Users));
  }
  /**
   * Create User.
   * @Rest\Post("/User")
   *
   * @return Response
   */
  public function postUserAction(Request $request)
  {
    $User = new User();
    $form = $this->createForm(UserType::class, $User);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($User);
      $em->flush();
      return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors()));
  }
  /**
     * Create User.
     * @Rest\Post("/Userlogin")
     *
     * @return Response
     */
    public function postUserLogin(Request $request)
    {
    $data = json_decode($request->getContent(), true);
    $name = $data['name'];
    $repository = $this->getDoctrine()->getRepository(User::class);
        $Users = $repository->findall(array("condition"=>"name = :name",'params'=> array(':name' => $name)));
        return $this->handleView($this->view($Users));
        return $this->handleView($this->view(['status' =>'ok'], Response::HTTP_OK));
        return $this->handleView($this->view($form->getErrors()));
    }
}