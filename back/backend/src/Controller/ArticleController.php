<?php
namespace App\Controller;
use App\Entity\articles;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    public function createArticle(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $articleTITLE = $request->request->get('title');
        $articCONTENT = $request->request->get('content');
        $articleAUTOR = $request->request->get('autor');
        $articleDATE =  new \DateTime();
        //$logger->info($articleTITLE);
        $article = new articles($articleTITLE, $articCONTENT, $articleAUTOR,$articleDATE);
        $em->persist($article);
        $em->flush();
        return new Response(
            "Article Crée",
            Response::HTTP_OK
        );
    }
    public function getArticleByTitle(Request $request, LoggerInterface $logger)
    {
        $title = $request->request->get('title');
        $allcontracts = $this->getDoctrine()->getRepository(articles::class)->find($title);
        return new Response(
           json_encode($allcontracts),
            Response::HTTP_OK
        );	
    }
    public function getallarticletitle(LoggerInterface $logger)
    {
        $repository = $this->getDoctrine()->getRepository(articles::class);
        $result = $repository->findall();
        $logger->info(json_encode($result));
        if (!$result) {
            throw $this->createNotFoundException(
                'No article found for this title'
            );
        }
        return json_encode($result);
    }
}
